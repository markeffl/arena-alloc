#![cfg_attr(not(test), no_std)]
#![deny(unsafe_op_in_unsafe_fn)]
#![deny(rust_2018_idioms)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]

use core::cell::{Cell, UnsafeCell};
use core::marker::PhantomData;
use core::mem::{self, MaybeUninit};

/// A zero-sized marker type with an [invariant] lifetime generic parameter.
///
/// [invariant]: https://doc.rust-lang.org/reference/subtyping.html#variance
#[derive(Debug, Clone, Copy)]
struct InvariantLifetime<'id>(PhantomData<*mut &'id ()>);

impl InvariantLifetime<'_> {
    /// Creates the marker for a specific lifetime.
    #[inline(always)]
    pub const fn new() -> Self {
        Self(PhantomData)
    }
}

/// The private inner type of a [`Slot`] in a [`Buffer`].
///
/// Every slot can be in one of three states:
///
/// * **free/uninitialized**: The slot has never been allocated and is entirely uninitialized.
///   Use `arena.init_count` to determine if a slot is initialized.
/// * **free/initialized**: The slot was allocated and has been freed.
///   In this state, `next` is the index of the next free slot and `value` is uninitialized.
/// * **allocated**: The slot is currently allocated and `value` is initialized.
///   In this state, `next` is `usize::MAX`.
struct SlotInner<T> {
    next: usize,
    value: MaybeUninit<T>,
}

/// A single storage cell in a [`Buffer`].
#[derive(Debug)]
pub struct Slot<T> {
    inner: UnsafeCell<MaybeUninit<SlotInner<T>>>,
}

impl<T> Slot<T> {
    /// An uninitialized slot.
    ///
    /// This value can be used to create an uninitialized buffer.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::Slot;
    ///
    /// let my_buffer = [Slot::<i32>::UNINIT; 256];
    /// ```
    pub const UNINIT: Self = Self {
        inner: UnsafeCell::new(MaybeUninit::uninit()),
    };
}

/// Constructs a new, [uninitialized](Self::UNINIT) slot.
impl<T> Default for Slot<T> {
    fn default() -> Self {
        Self::UNINIT
    }
}

/// The error returned by [`Buffer::try_grow`] if the operation fails.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct GrowError;

/// A trait for types that can be used as the backing memory for [`Arena`].
///
/// The buffer is semantically similar to a slice of [`Slot`].
/// It *should* allow access to a slot in constant time (O(n)).
/// It *optionally* supports growing the buffer, but if it does, then this operation *must not*
/// move the existing slots in memory.
///
/// # Safety
///
/// This trait in unsafe to implement, because [`Arena`] makes certain assumptions about [`Self::get`].
/// The implementor of this trait *must* guarantee all of the following:
///
/// * For every `buffer`: `buffer.get(index1)` and `buffer.get(index2)` *must* return a reference
///   to the same slot (memory location) if and only if `index1 == index2`.
///   This implies that [`Self::try_grow`] cannot move the "front" of the buffer in memory.
/// * If two buffers can be used concurrently, it *must not* be possible to obtain a reference
///   to the same slot (memory location) from both buffers.
///   This usually means that the buffer has exclusive access to the underlying memory.
///   In particular, a shared slice (`&[Slot<T>]`) cannot be a buffer.
///
/// For the sake of memory safety, there are no special restrictions for [`Self::capacity`] and
/// [`Self::try_grow`], but incorrect implementations may lead to panics.
pub unsafe trait Buffer {
    /// The type of the elements contained in the slots of the buffer.
    type Item;

    /// Returns a reference to a slot in the buffer.
    ///
    /// # Panics
    ///
    /// This *may* panic if `index >= self.capacity()`.
    fn get(&self, index: usize) -> &Slot<Self::Item>;

    /// Returns total number of slots in the buffer.
    fn capacity(&self) -> usize;

    /// Tries to reserve at least one extra slot in the buffer.
    ///
    /// # Errors
    ///
    /// Returns an error if the buffer is at maximum capacity.
    fn try_grow(&self) -> Result<(), GrowError> {
        Err(GrowError)
    }
}

// SAFETY: An exclusive slice has exclusive access to the memory and the results of `get` cannot overlap.
unsafe impl<T> Buffer for &mut [Slot<T>] {
    type Item = T;

    fn get(&self, index: usize) -> &Slot<Self::Item> {
        &self[index]
    }

    fn capacity(&self) -> usize {
        self.len()
    }
}

/// A token that represents ownership of an allocated value in an [`Arena`].
///
/// Each token is associated with a specific arena by the "branded" lifetime parameter `'id`.
///
/// The value can be accessed with [`Arena::get`] or [`Arena::get_mut`].
#[derive(Debug)] // not Copy/Clone
pub struct AccessToken<'id> {
    index: usize,
    _marker: InvariantLifetime<'id>,
}

/// A token that allows the construction of an [`Arena`] with the brand `'id`.
///
/// The construction of the brand is decoupled from the construction of the arena to allow the
/// buffer to depend on the brand.
/// This allows the buffer to store local lifetimes of the closure, including brand lifetimes.
#[derive(Debug)] // not Copy/Clone
pub struct CreateToken<'id> {
    _marker: InvariantLifetime<'id>,
}

impl CreateToken<'_> {
    /// Creates a token with an unique brand and calls the given closure with the token.
    pub fn enter<R>(callback: impl for<'id> FnOnce(CreateToken<'id>) -> R) -> R {
        let token = CreateToken {
            _marker: InvariantLifetime::new(),
        };
        callback(token)
    }
}

/// A region-based memory allocator.
///
/// The arena is backed by a [`Buffer`] and uses a lifetime-branded [`AccessToken`] to safely
/// access and deallocate the allocated objects.
#[derive(Debug)]
pub struct Arena<'id, B: Buffer> {
    buffer: B,
    /// The number of initialized slots at the front of the buffer.
    /// The slot at `index` is initialized if and only if `index < init_count`.
    init_count: Cell<usize>,
    /// The index of a free slot in the buffer, or one past the end if the buffer is full.
    /// Use `init_count` to determine if the slot is initialized.
    free_index: Cell<usize>,
    _marker: InvariantLifetime<'id>,
}

/// Drops all allocated values in an unspecified order.
impl<B: Buffer> Drop for Arena<'_, B> {
    fn drop(&mut self) {
        if !mem::needs_drop::<B::Item>() {
            // Skip cross-crate code generation for destructor if it does nothing.
            return;
        }

        for index in 0..self.init_count.get() {
            let slot_inner = self.get_inner(index);
            // SAFETY: The slots in the range `0..init_count` are initialized.
            // If `slot.next == usize::MAX`, then the slot is allocated and `slot.value` is initialized.
            // Since we're in the destructor, we're allowed to drop the values.
            unsafe {
                if (*slot_inner).next == usize::MAX {
                    (*slot_inner).value.assume_init_drop();
                }
            }
        }
    }
}

impl<'id, B: Buffer> Arena<'id, B> {
    /// Consumes a creation token to create a new arena backed by the given buffer.
    ///
    /// Consuming the token ensures that the arena has an unique brand.
    pub const fn new(_token: CreateToken<'id>, buffer: B) -> Self {
        Self {
            buffer,
            init_count: Cell::new(0),
            free_index: Cell::new(0),
            _marker: InvariantLifetime::new(),
        }
    }

    /// Allocates a memory location and initializes it with the provided value.
    ///
    /// On success, returns an [`AccessToken`] that can be used to access the value.
    ///
    /// # Errors
    ///
    /// If the backing buffer reports out-of-memory, the input value is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 2];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let _ = arena.alloc(1).unwrap();
    ///     let _ = arena.alloc(2).unwrap();
    ///     let _ = arena.alloc(3).unwrap_err();
    /// });
    /// ```
    pub fn alloc(&self, value: B::Item) -> Result<AccessToken<'id>, B::Item> {
        let index = self.free_index.get();
        assert!(index < usize::MAX, "capacity overflow");
        if index >= self.buffer.capacity() {
            match self.buffer.try_grow() {
                Ok(()) => (),
                Err(GrowError) => return Err(value),
            }
        }

        let slot_inner = self.get_inner(index);

        let init_count = self.init_count.get();
        if index < init_count {
            // Free slot is initialized, point `free_index` to the next free slot in the chain.
            // SAFETY: The slot is initialized, because `index < init_count` and free, because
            // `self.free_index` points to the slot.
            // Therefore, `slot_inner.next` points to the next free slot (or one after the end).
            let next_free = unsafe { (*slot_inner).next };
            self.free_index.set(next_free);
        } else {
            // Free slot is uninitialized, point `free_index` to the next slot in the buffer,
            // which is also uninitialized.
            // Also increase the initialized count, because one more slot will be initialized below.
            debug_assert_eq!(
                index, init_count,
                "expected uninitialized element to be one past the initialized elements",
            );
            self.free_index.set(index + 1);
            self.init_count.set(init_count + 1);
        }

        let slot_value = SlotInner {
            next: usize::MAX,
            value: MaybeUninit::new(value),
        };

        // SAFETY: This slot that we got from `self.free_index` is free and we made sure to
        // update `self.free_index` to a different free slot above.
        unsafe {
            slot_inner.write(slot_value);
        }

        Ok(AccessToken {
            index,
            _marker: InvariantLifetime::new(),
        })
    }

    /// Returns a reference to the allocated value for the given token.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 16];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let access = arena.alloc(42).unwrap();
    ///     let value = arena.get(&access);
    ///     assert_eq!(*value, 42);
    /// });
    /// ```
    pub fn get<'a>(&'a self, token: &'a AccessToken<'id>) -> &'a B::Item {
        let inner = self.get_inner(token.index);
        // SAFETY: Since we have an `AccessToken` with the same `'id` as `Self`, the slot at
        // its index must be allocated and initialized.
        // The returned memory location is valid for the lifetime `'a`, because `self` and
        // therefore `self.buffer` outlive `'a`.
        // The shared borrow of `token` ensures that the same slot cannot be exclusively
        // borrowed or freed for the lifetime `'a`.
        unsafe {
            debug_assert_eq!((*inner).next, usize::MAX);
            (*inner).value.assume_init_ref()
        }
    }

    /// Returns a mutable reference to the allocated value for the given token.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 16];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let mut access = arena.alloc(40).unwrap();
    ///     let value = arena.get_mut(&mut access);
    ///     *value += 2;
    ///     assert_eq!(*value, 42);
    /// });
    /// ```
    pub fn get_mut<'a>(&'a self, token: &'a mut AccessToken<'id>) -> &'a mut B::Item {
        let inner = self.get_inner(token.index);
        // SAFETY: See above for why the slot access is valid.
        // The exclusive borrow of `token` ensures that the same slot cannot be borrowed again
        // (shared or exclusively) or freed for the lifetime `'a`.
        unsafe {
            debug_assert_eq!((*inner).next, usize::MAX);
            (*inner).value.assume_init_mut()
        }
    }

    /// Consumes a token and returns a mutable reference to the allocated value.
    ///
    /// The returned reference will live as long as the arena.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 16];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let access = arena.alloc(40).unwrap();
    ///     let value = arena.leak(access);
    ///     *value += 2;
    ///     assert_eq!(*value, 42);
    /// });
    /// ```
    #[allow(clippy::mut_from_ref)]
    pub fn leak<'a>(&'a self, token: AccessToken<'id>) -> &'a mut B::Item {
        let inner = self.get_inner(token.index);
        // SAFETY: See above for why the slot access is valid.
        // Since we consume the `token` by value, we have exclusive access to the slot and
        // the slot can never be accessed or freed again.
        unsafe {
            debug_assert_eq!((*inner).next, usize::MAX);
            (*inner).value.assume_init_mut()
        }
    }

    /// Consumes a token and returns the associated value.
    ///
    /// This transfers ownership of the value to the caller and frees the storage location in
    /// the arena for future allocations.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 16];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let access = arena.alloc(42).unwrap();
    ///     let value = arena.take(access);
    ///     assert_eq!(value, 42);
    /// });
    /// ```
    pub fn take(&self, token: AccessToken<'id>) -> B::Item {
        let inner = self.get_inner(token.index);
        let free_index = self.free_index.get();
        // SAFETY: See above for why the slot access is valid.
        // Since we consume the `token` by value, we have exclusive access to the slot and are
        // therefore allowed to transfer ownership of the value and mark the slot as free.
        let value = unsafe {
            debug_assert_eq!((*inner).next, usize::MAX);
            (*inner).next = free_index;
            (*inner).value.assume_init_read()
        };
        self.free_index.set(token.index);
        value
    }

    /// Deallocates the value for the given token.
    ///
    /// This is semantically equivalent to `drop(arena.take(token))`, but might be implemented
    /// more efficiently.
    ///
    /// # Examples
    ///
    /// ```
    /// use arena_alloc::{Arena, CreateToken, Slot};
    ///
    /// CreateToken::enter(|create| {
    ///     let mut buffer = [Slot::UNINIT; 16];
    ///     let arena = Arena::new(create, buffer.as_mut_slice());
    ///     let access = arena.alloc(40).unwrap();
    ///     arena.free(access);
    /// });
    /// ```
    pub fn free(&self, token: AccessToken<'id>) {
        let inner = self.get_inner(token.index);
        let free_index = self.free_index.get();
        // SAFETY: See above for why the slot access is valid.
        // Since we consume the `token` by value, we have exclusive access to the slot and are
        // therefore allowed to drop the value and mark the slot as free.
        unsafe {
            debug_assert_eq!((*inner).next, usize::MAX);
            (*inner).next = free_index;
            (*inner).value.assume_init_drop()
        }
        self.free_index.set(token.index);
    }

    /// Returns an unsafe pointer to the slot at the given index.
    ///
    /// Since the slot is inside an [`UnsafeCell`], we're allowed to write to this pointer, if
    /// we ensure exclusive access.
    fn get_inner(&self, index: usize) -> *mut SlotInner<B::Item> {
        self.buffer.get(index).inner.get().cast()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::rc::Rc;

    #[test]
    fn basic_alloc_get_free() {
        CreateToken::enter(|create| {
            #[derive(Debug, PartialEq, Eq)]
            struct S(i32);

            let mut buffer = [Slot::UNINIT; 2];
            let arena = Arena::new(create, buffer.as_mut_slice());

            let mut x = arena.alloc(S(1)).unwrap();
            let y = arena.alloc(S(2)).unwrap();

            assert_eq!(*arena.get(&x), S(1));
            assert_eq!(*arena.get(&y), S(2));

            *arena.get_mut(&mut x) = S(3);
            assert_eq!(*arena.get(&x), S(3));

            arena.alloc(S(4)).unwrap_err();

            arena.free(y);
            let z = arena.alloc(S(4)).unwrap();
            assert_eq!(*arena.get(&z), S(4));
            assert_eq!(*arena.get(&x), S(3));
            assert_eq!(arena.take(x), S(3));
        });
    }

    #[test]
    fn destructor() {
        #[derive(Debug)]
        struct DropCounter(Rc<Cell<i32>>);
        impl Drop for DropCounter {
            fn drop(&mut self) {
                self.0.set(self.0.get() + 1);
            }
        }

        let mut buffer = [Slot::UNINIT; 4];

        // explicit free
        CreateToken::enter(|create| {
            let arena = Arena::new(create, buffer.as_mut_slice());
            let counter = Rc::new(Cell::new(0));

            let token = arena.alloc(DropCounter(counter.clone())).unwrap();
            assert_eq!(counter.get(), 0);

            arena.free(token);
            assert_eq!(counter.get(), 1);
        });

        // implicit drop
        CreateToken::enter(|create| {
            let arena = Arena::new(create, buffer.as_mut_slice());
            let counter = Rc::new(Cell::new(0));

            let _ = arena.alloc(DropCounter(counter.clone())).unwrap();
            assert_eq!(counter.get(), 0);

            drop(arena);
            assert_eq!(counter.get(), 1);
        });

        // mixed free + drop
        CreateToken::enter(|create| {
            let arena = Arena::new(create, buffer.as_mut_slice());
            let counter1 = Rc::new(Cell::new(0));
            let counter2 = Rc::new(Cell::new(0));

            let token1 = arena.alloc(DropCounter(counter1.clone())).unwrap();
            let _token2 = arena.alloc(DropCounter(counter2.clone())).unwrap();
            assert_eq!(counter1.get(), 0);
            assert_eq!(counter2.get(), 0);

            arena.free(token1);
            assert_eq!(counter1.get(), 1);
            assert_eq!(counter2.get(), 0);

            drop(arena);
            assert_eq!(counter1.get(), 1);
            assert_eq!(counter2.get(), 1);
        });
    }
}

#[cfg(doctest)]
mod compile_fail {
    //! compile-fail tests
    //!
    //! # Escaping closure
    //!
    //! ```compile_fail
    //! use arena_alloc::CreateToken;
    //!
    //! let token = CreateToken::enter(|token| token);
    //! ```
    //!
    //! # Access after dropping arena
    //!
    //! ```compile_fail,E0505
    //! use arena_alloc::{Arena, CreateToken, Slot};
    //!
    //! CreateToken::enter(|create| {
    //!     let mut buffer = [Slot::UNINIT; 4];
    //!     let arena = Arena::new(create, buffer.as_mut_slice());
    //!     let access = arena.alloc(42).unwrap();
    //!     let value = arena.get(&access);
    //!     drop(arena);
    //!     drop(value);
    //! });
    //! ```
    //!
    //! # Access after dropping token
    //!
    //! ```compile_fail,E0505
    //! use arena_alloc::{Arena, CreateToken, Slot};
    //!
    //! CreateToken::enter(|create| {
    //!     let mut buffer = [Slot::UNINIT; 4];
    //!     let arena = Arena::new(create, buffer.as_mut_slice());
    //!     let access = arena.alloc(42).unwrap();
    //!     let value = arena.get(&access);
    //!     drop(access);
    //!     drop(value);
    //! });
    //! ```
    //!
    //! # Branding
    //!
    //! ```compile_fail,E0521
    //! use arena_alloc::{Arena, CreateToken, Slot};
    //!
    //! CreateToken::enter(|create1| {
    //!     CreateToken::enter(|create2| {
    //!         let mut buffer1 = [Slot::<i32>::UNINIT; 4];
    //!         let mut buffer2 = [Slot::<i32>::UNINIT; 4];
    //!         let arena1 = Arena::new(create1, buffer1.as_mut_slice());
    //!         let arena2 = Arena::new(create2, buffer2.as_mut_slice());
    //!         let access = arena1.alloc(42).unwrap();
    //!         let _ = arena2.get(&access);
    //!     });
    //! });
    //! ```
    //!
    //! ```compile_fail,E0521
    //! use arena_alloc::{Arena, CreateToken, Slot};
    //!
    //! CreateToken::enter(|create1| {
    //!     CreateToken::enter(|create2| {
    //!         let mut buffer1 = [Slot::<i32>::UNINIT; 4];
    //!         let mut buffer2 = [Slot::<i32>::UNINIT; 4];
    //!         let arena1 = Arena::new(create1, buffer1.as_mut_slice());
    //!         let arena2 = Arena::new(create2, buffer2.as_mut_slice());
    //!         let access = arena2.alloc(42).unwrap();
    //!         let _ = arena1.get(&access);
    //!     });
    //! });
    //! ```
}
