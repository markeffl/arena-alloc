use arena_alloc::{AccessToken, Arena, Buffer, CreateToken, Slot};

#[derive(Debug)]
struct Node<'id, T> {
    value: T,
    next: Option<AccessToken<'id>>,
}

#[derive(Debug)]
struct SinglyLinkedList<'a, 'id, B: Buffer> {
    arena: &'a Arena<'id, B>,
    head: Option<AccessToken<'id>>,
}

impl<'a, 'id, T, B: Buffer<Item = Node<'id, T>>> SinglyLinkedList<'a, 'id, B> {
    fn new(arena: &'a Arena<'id, B>) -> Self {
        Self { arena, head: None }
    }

    fn push(&mut self, value: T) {
        let node = Node {
            value,
            next: self.head.take(),
        };
        let token = self.arena.alloc(node).ok().expect("out of memory");
        self.head = Some(token);
    }

    fn pop(&mut self) -> Option<T> {
        let Some(token) = self.head.take() else { return None };
        let node = self.arena.take(token);
        self.head = node.next;
        Some(node.value)
    }
}

#[test]
fn test() {
    CreateToken::enter(|create| {
        let mut buffer = [Slot::UNINIT; 10];
        let arena = Arena::new(create, buffer.as_mut_slice());
        let mut list = SinglyLinkedList::new(&arena);

        for i in 0..10 {
            list.push(i);
        }

        for i in (0..10).rev() {
            assert_eq!(list.pop(), Some(i));
        }

        assert_eq!(list.pop(), None);
    });
}
